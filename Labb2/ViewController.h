//
//  ViewController.h
//  Labb2
//
//  Created by Viktor Jegerås on 2015-01-26.
//  Copyright (c) 2015 Viktor Jegerås. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UISegmentedControl *genre;

@property (weak, nonatomic) IBOutlet UISwitch *gender;


@end


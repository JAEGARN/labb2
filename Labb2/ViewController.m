//
//  ViewController.m
//  Labb2
//
//  Created by Viktor Jegerås on 2015-01-26.
//  Copyright (c) 2015 Viktor Jegerås. All rights reserved.
//
#import "TheStory.h"
#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextView *storyText;
@property (weak, nonatomic) IBOutlet UISwitch *ending;
@property (weak, nonatomic) IBOutlet UISwitch *background;

@end

@implementation ViewController

- (IBAction)generate:(id)sender {
    TheStory *story = [[TheStory alloc] init];
    [story setStory:[self getSelGenre]withHappyEnding:[self isEndingHappy] andBackground:[self isBackgroundOn]];
    self.storyText.text = [story storyTeller];
}


-(int)getSelGenre{
    return self.genre.selectedSegmentIndex;
}

-(BOOL)isEndingHappy{
    return self.ending.isOn;
}

-(BOOL)isBackgroundOn{
    return self.background.isOn;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end

//
//  TheStory.h
//  Labb2
//
//  Created by Viktor Jegerås on 2015-01-26.
//  Copyright (c) 2015 Viktor Jegerås. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TheStory : NSObject

@property(nonatomic) NSMutableArray *person;
@property(nonatomic) NSMutableArray *building;
@property(nonatomic) NSMutableArray *surrounding;
@property(nonatomic) NSMutableArray *badGuys;
@property(nonatomic) NSMutableArray *disaster;

-(void) setStory:(int)genre withHappyEnding:(BOOL)ending andBackground:(BOOL)back;



-(NSString*) storyTeller;

@end

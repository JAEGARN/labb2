//
//  TheStory.m
//  Labb2
//
//  Created by Viktor Jegerås on 2015-01-26.
//  Copyright (c) 2015 Viktor Jegerås. All rights reserved.
//

#import "TheStory.h"


@implementation TheStory

    - (instancetype)init
    {
        self = [super init];
        if (self) {
            self.person = [[NSMutableArray alloc]init];
            self.building = [[NSMutableArray alloc]init];
            self.surrounding = [[NSMutableArray alloc]init];
            self.badGuys = [[NSMutableArray alloc]init];
            self.disaster = [[NSMutableArray alloc]init];
        }
        return self;
    }
    NSString *theEnding;
    NSString *theBackground;
-(void)setStory:(int)genre withHappyEnding:(BOOL)ending andBackground:(BOOL)back{
        if(genre == 0){
            [_person addObject:@"Dracula"];
            [_person addObject:@"Bee-Bop"];
            [_person addObject:@"Rocksteady"];
            [_person addObject:@"Alucard"];
            [_person addObject:@"Shredder"];
            [_building addObject:@"i ett mörkt hemskt slott"];
            [_building addObject:@"i en djup håla i skogen"];
            [_building addObject:@"i helvetes eviga eldar"];
            [_surrounding addObject:@"ett hemskt åsköväder"];
            [_surrounding addObject:@"en fullmåne"];
            [_surrounding addObject:@"en bloddroppande natt"];
            [_badGuys addObject:@"Van Helsing"];
            [_badGuys addObject:@"Bävern"];
            [_badGuys addObject:@"Splinter"];
            if (back) {
                theBackground = @"Skräck är en vid genre som innehåller allt från terror till horror, som är två subgenrer inom denna litteratur, dock med varsitt syfte. Den ena är till för de mera blodiga, lemlästande skräckhistorierna och den andra för de mer djupgående psykologiska, melankoliska";
            }
        }else if(genre == 1){
            [_person addObject:@"Kalle Anka"];
            [_person addObject:@"Musse Pigg"];
            [_person addObject:@"Tintin"];
            [_person addObject:@"Knasen"];
            [_person addObject:@"Herman Hedning"];
            [_building addObject:@"Ankeborg"];
            [_building addObject:@"på Skaraslätten"];
            [_building addObject:@"i barracken"];
            [_surrounding addObject:@"en glimrande sjö"];
            [_surrounding addObject:@"en köttbullsregnande stad"];
            [_surrounding addObject:@"skjutbanan"];
            [_badGuys addObject:@"BjörnLigan"];
            [_badGuys addObject:@"Schassen"];
            [_badGuys addObject:@"Kaninen"];
            if (back) {
                theBackground = @"En serietidning eller ett seriemagasin, är en tidning som i huvudsak innehåller tecknade serier. Jämte serier som producerat för dagspressen - dagspresserier - så har serietidningen traditionellt sett varit det huvudsakliga formatet för tecknade serier. De första serietidningarna publicerades på 1930-talet - som historiens första serietidning brukar nämnas den amerikanska Famous Funnies, utgiven från 1934 till 1955.";
            }

        }else if(genre == 2){
            [_person addObject:@"Fantomen"];
            [_person addObject:@"Batman"];
            [_person addObject:@"Wolverine"];
            [_person addObject:@"Leonardo"];
            [_person addObject:@"Splinter"];
            [_building addObject:@"dödskallegrottan"];
            [_building addObject:@"kloakerna"];
            [_building addObject:@"batcave"];
            [_building addObject:@"New York City"];
            [_surrounding addObject:@"tunnelbanan"];
            [_surrounding addObject:@"djungeln"];
            [_surrounding addObject:@"kloakerna"];
            [_badGuys addObject:@"Shredder"];
            [_badGuys addObject:@"Jokern"];
            [_badGuys addObject:@"Mr.Freeze(Arnold)"];
            [_badGuys addObject:@"Gåtan"];
            [_badGuys addObject:@"Crang"];
            if (back) {
                theBackground = @"Hjälte är i allmän betydelse en beundransvärd person som utför stordåd, och som på så sätt vinner stor heder och ära. I Grekisk mytologi kallades hjältarna heroer (ἥρωι), varifrån många språk fått sitt ord för hjälte, liksom svenska orden heroisk och heroism (hjältemod). De grekiska heroerna hade ofta gudomligt påbrå, vilket är vanligt även bland hjältar i andra mytologier. Hjälte förknippas med Fröken Kristina";
            }else if (!back) {
                theBackground = @" ";
            }
       
        }
        if (ending) {
            [_disaster addObject:@"en stor bukett blommor som luktade ljuvligt"];
            [_disaster addObject:@"lös sina aggretioner på varandra"];
            [_disaster addObject:@"en bomb som inte gjorde någon skada utan färgade allt i regnbågens alla färger"];
            theEnding = @"alla överlevde och var lyckliga resten av sina liv";
        }else{
            [_disaster addObject:@"en stor vätebomb"];
            [_disaster addObject:@"lös Godzilla som gick bananZ"];
            [_disaster addObject:@"lös vädergudarnas makter så det kom en tyfon"];
            [_disaster addObject:@"ett virus så att alla blev Zombies"];
            theEnding = @"alla dog och deras vänner var olyckliga resten av sina liv";
        }
    
    
        
    }

    -(NSString*) storyTeller{
        NSString *p = [_person objectAtIndex:arc4random() %([_person count])];
        NSString *b = [_building objectAtIndex:arc4random() %([_building count])];
        NSString *s = [_surrounding objectAtIndex:arc4random() %([_surrounding count])];
        NSString *bad = [_badGuys objectAtIndex:arc4random() %([_surrounding count])];
        NSString *badSec = [_badGuys objectAtIndex:arc4random() %([_surrounding count])];
        NSString *pSec = [_person objectAtIndex:arc4random() %([_person count])];
        NSString *bSec = [_building objectAtIndex:arc4random() %([_building count])];
        NSString *sSec = [_surrounding objectAtIndex:arc4random() %([_surrounding count])];
        NSString *dis = [_disaster objectAtIndex:arc4random() %([_surrounding count])];
        
        NSString *story = [NSString stringWithFormat:@"Det var en gång %@, Han bodde %@. Han tittade ut över %@. En dag träffade han %@ och dem båda gick till %@, där var dem och tittade på %@ där såg de den ondskefulla %@ och den ännu mer ondskefulla %@. dom båda skurkarna släppte %@ och %@. %@" , p, b, s, pSec, bSec, sSec, bad, badSec, dis, theEnding, theBackground];
        
        return story;

    };




@end
